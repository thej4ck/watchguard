Watchguard
----------

This is a simple class i needed for scheduling tasks in ESP8266 and Arduino in general.

It calls a callback function at regular times, specified in milliseconds.

Whatchguard foo(5000, bar())
//run bar() function every 5 second.

foo.loop()

is needed for checking wheater or not bar() has to be fired or not.
avoid to use delay() in the rest of your program, or at least to not
use delay for amount of time bigger than the interval you set in the constructor (es. 5000).


