#ifndef Watchguard_h
#define Watchguard_h


class Watchguard
{
  public:
    Watchguard(long ms, void (*CallbackFunction)());
    void loop();
  
  private:
    void (*CallbackFunction)();
    long _ms;
    long _last;
};

#endif