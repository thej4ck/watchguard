#include "Arduino.h"
#include "Watchguard.h"

Watchguard::Watchguard(long ms, void (*CallbackFunction)())
{
  this->CallbackFunction = CallbackFunction;
  _ms = ms;
  _last = millis();
}

void Watchguard::loop()
{
  long now = millis();
  if (now - _last > _ms ) {
    _last = millis();
    CallbackFunction();
  }
}